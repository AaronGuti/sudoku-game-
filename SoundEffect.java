import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
/**
 * Class to produce sound effects 
 * @author Joren, Kinsley, Aaron
 *
 */
public class SoundEffect{
	private ArrayList<File> files = new ArrayList<File>(); 

	/**
	 * Constructor adding sound files to the array list of files
	 */
	public SoundEffect() {
		files.add(new File("./Sounds/Roblox.wav").getAbsoluteFile()); 
		files.add(new File("./Sounds/Darth Vader.wav").getAbsoluteFile()); 
		files.add(new File("./Sounds/Pac Man.wav").getAbsoluteFile());
		files.add(new File("./Sounds/Waluigi.wav").getAbsoluteFile());
		files.add(new File("./Sounds/Confusion.wav").getAbsoluteFile());
		files.add(new File("./Sounds/Skrra.wav").getAbsoluteFile());
	}
	
	
	/**
	 * Picks a random random file from the array list of files
	 * @return
	 * 			File with which to be played
	 */
	public File pickRandomFile() { 
		Random rand = new Random();
		int randomInt = rand.nextInt(files.size()); 
		return files.get(randomInt); 
	}
	
	/**
	 * Plays a randomly selected file from the array list of files
	 */
	public void playSound() {
		File file = pickRandomFile(); 
		try {
        		AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file);
        		Clip clip;
			clip = AudioSystem.getClip();
			clip.open(audioInputStream);
        		clip.start();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}