import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import javax.print.attribute.standard.Media;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

@SuppressWarnings("serial")
public class GameBoard extends JFrame {
	private JMenu fileMenu;
	private EventHandler eh = new EventHandler();
	protected Square[][] squares = new Square[9][9];
	private JMenuBar menuBar;
	private ArrayList<String> availableHelpList = new ArrayList<>();

	/**
	 * Creates a game board and makes it visible
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		GameBoard GameBoard = new GameBoard();
		GameBoard.setVisible(true);
	}

	/**
	 * Creates a game board of size 500x500 and allows the user to enter values
	 * between 1-9 in each tile. If an invalid character is entered in a tile then a
	 * sound effect will be played to let the user know that it is invalid. If a
	 * user hovers over a tile then the numbers that can be use are displayed.
	 */
	public GameBoard() {
		SoundEffect sounds = new SoundEffect();
		setTitle("Sudoku Helper");
		setSize(500, 500);
		JPanel container = new JPanel();
		container.setLayout(new BorderLayout());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel board = new JPanel();
		board.setLayout(new GridLayout(9, 9));
		buildMenu();
		ArrayList<Character> validNumbers = new ArrayList<>();
		for (int i = 0; i < 9; i++) {
			validNumbers.add(Character.forDigit(i + 1, 10));
		}
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				Square square = new Square(i, j, 0);
				square.addKeyListener(new KeyAdapter() {
					@Override
					public void keyTyped(KeyEvent keyEvent) {
						char keyPressed = keyEvent.getKeyChar();
						Square square = (Square) keyEvent.getSource();
						for (int k = 0; k < 9; k++) {
							for (int l = 0; l < 9; l++) {
								if (square.compareSquare(squares[l][k])) {
									if (squares[l][k].getText().equals(Character.toString(keyPressed))
											|| !validNumbers.contains(keyPressed) || !square.getText().equals("")) {
										sounds.playSound();
										keyEvent.consume();
									}
								}
							}
						}
						helper(squares);
					}

					@Override
					public void keyReleased(KeyEvent keyEvent) {
						helper(squares);
					}
				});
				squares[i][j] = square;
				board.add(square);
			}
		}
		helper(squares);
		container.add(board);
		this.add(container);
	}

	/**
	 * Creates a menu from which the user can pick an option to save, create a new
	 * board, open a saved board, exit, print the current board
	 */
	public void buildMenu() {
		menuBar = new JMenuBar();
		fileMenu = new JMenu("File");
		JMenuItem menuItem = new JMenuItem("New");
		menuItem.addActionListener(eh);
		menuItem.setAccelerator(KeyStroke.getKeyStroke('N', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		fileMenu.add(menuItem);
		menuItem = new JMenuItem("Open");
		menuItem.addActionListener(eh);
		menuItem.setAccelerator(KeyStroke.getKeyStroke('O', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		fileMenu.add(menuItem);
		menuItem = new JMenuItem("Save");
		menuItem.addActionListener(eh);
		menuItem.setAccelerator(KeyStroke.getKeyStroke('S', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		fileMenu.add(menuItem);
		menuItem = new JMenuItem("Exit");
		menuItem.addActionListener(eh);
		menuItem.setAccelerator(KeyStroke.getKeyStroke('X', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		fileMenu.add(menuItem);
		menuBar.add(fileMenu);

		// **********************************
		menuItem = new JMenuItem("Victory");
		menuItem.addActionListener(eh);
		menuItem.setAccelerator(KeyStroke.getKeyStroke('V', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		fileMenu.add(menuItem);
		// *************************************
		// *************************************
		menuItem = new JMenuItem("Print");
		menuItem.addActionListener(eh);
		menuItem.setAccelerator(KeyStroke.getKeyStroke('P', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		fileMenu.add(menuItem);
		// ****************************************
		// *************************************
		menuItem = new JMenuItem("Solve");
		menuItem.addActionListener(eh);
		menuItem.setAccelerator(KeyStroke.getKeyStroke('C', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		fileMenu.add(menuItem);
		// ****************************************
		setJMenuBar(menuBar);
	}

	/**
	 * Creates a label over a tile that lists available options to be entered into
	 * the tile.
	 * 
	 * @param board
	 */
	private void helper(Square[][] board) {

		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				Square square = board[i][j];
				availableHelpList.clear();
				for (int k = 0; k < 9; k++) {
					availableHelpList.add(Integer.toString(k + 1));
				}
				for (int l = 0; l < 9; l++) {
					for (int m = 0; m < 9; m++) {
						if (square.compareSquare(board[l][m])) {
							if (availableHelpList.contains(board[l][m].getText())) {
								availableHelpList.remove(availableHelpList.indexOf(board[l][m].getText()));
							}
						}
						square.setToolTipText(availableHelpList.toString());
					}
				}
			}
		}
		if (availableHelpList.isEmpty()) {
			Victory();
		}
	}

	/**
	 * Prints out that the helperList is empty to the console (or the helper label)
	 * when the game is won.
	 */
	private void Victory() {
		System.out.println("List is empty");
	}

	/**
	 * Assigns corresponding behavior to each menu option
	 * 
	 * @author Joren, Kinsley, Aaron
	 *
	 */
	public class EventHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (arg0.getActionCommand().equals("New")) {
				clearGameBoard();
				helper(squares);
			}
			if (arg0.getActionCommand().equals("Save")) {
				saveSudokuFile();
			}
			if (arg0.getActionCommand().equals("Open")) {
				JFileChooser fileChooser = new JFileChooser("./Resources");
				int returnVal = fileChooser.showOpenDialog(GameBoard.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					openSudokuFile(file);
				}
			}
			if (arg0.getActionCommand().equals("Exit")) {
				System.exit(0);
			}
			if (arg0.getActionCommand().equals("Victory")) {
				Victory();
			}
			if (arg0.getActionCommand().equals("Print")) {
				printSquares();
			}
			if (arg0.getActionCommand().equals("Solve")) {
				System.out.println("Solving...");
			}
		}
	}

	/**
	 * Sets all tiles to blank characters
	 */
	public void clearGameBoard() {
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				squares[i][j].setText("");
			}
		}
	}

	/**
	 * Saves the Sudoku game as a 9x9 two-dimensional array. Blank tiles will be set
	 * as zeros.
	 */
	public void saveSudokuFile() {
		BufferedWriter writer = null;
		JFileChooser fileChooser = new JFileChooser("./Resources");
		int saveFile = fileChooser.showSaveDialog(GameBoard.this);
		if (saveFile == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			try {
				writer = new BufferedWriter(new FileWriter(file));
				for (int i = 0; i < 9; i++) {
					for (int j = 0; j < 9; j++) {
						if (squares[i][j].getText().equals("")) {
							writer.write("0");
						}
						writer.write(squares[i][j].getText());
						if (j == 8) {
							writer.newLine();
						} else {
							writer.write(" ");
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Iterates over a file and replaces each blank tile with the corresponding
	 * value within the file.
	 * 
	 * @param file
	 *            the file to be read
	 */
	public void openSudokuFile(File file) {
		clearGameBoard();
		int[] savedTiles;
		try {
			java.util.List<Integer> savedIntegerList = new ArrayList<Integer>();
			@SuppressWarnings("resource")
			Scanner scanner = new Scanner(file);
			while (scanner.hasNextInt()) {
				savedIntegerList.add(scanner.nextInt());
			}
			savedTiles = new int[savedIntegerList.size()];
			Iterator<Integer> iterator = savedIntegerList.iterator();
			int i = 0;
			while (iterator.hasNext()) {
				savedTiles[i] = iterator.next();
				i++;
			}
		} catch (FileNotFoundException e) {
			System.out.println("File " + file.getName() + " not found!");
			savedTiles = new int[0];
		}
		for (int i = 0; i < savedTiles.length; i++) {
			if (savedTiles[i] != 0) {
				squares[i / 9][i % 9].setText(Integer.toString(savedTiles[i]));
				squares[i / 9][i % 9].setNumber(Integer.toString(savedTiles[i]));
			}
		}
		helper(squares);
	}

	/**
	 * This method prints out the current state of the game into the console.
	 * 
	 * @return a two-dimensional array containing the values of each tile in the
	 *         game
	 */
	public int[][] printSquares() {
		int[][] toReturn = new int[9][9];
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (j < 8) {
					if (squares[i][j].getText().equals("")) {
						System.out.print("0 ");
						toReturn[i][j] = 0;
					} else {
						System.out.print(squares[i][j].getText() + " ");
						toReturn[i][j] = Integer.parseInt(squares[i][j].getText());
					}
				} else {
					if (squares[i][j].getText().equals("")) {
						System.out.print("0");
						System.out.print("\n");
						toReturn[i][j] = 0;
					} else {
						System.out.print(squares[i][j].getText() + "");
						System.out.print("\n");
						toReturn[i][j] = Integer.parseInt(squares[i][j].getText());
					}
				}
			}
		}
		return toReturn;
	}
}