import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JTextField;
/**
 * Class representing a square object within out sudoku helper
 * @author Joren, Kinsley, Aaron
 *
 */
public class Square extends JTextField {
    private final int row;
    private final int column;
    private final int tile;
    protected int number;
    
    /**
     * Constructor creating a square object at a given location with a given value
     * @param rowInput
     * 					integer value representing the row location of this square
     * @param columnInput
     * 					integer value representing the column location of this square
     * @param passedNumber
     * 					integer value representing the number value held in this square
     */
    public Square(int rowInput, int columnInput, int passedNumber) {
        row = rowInput;
        column = columnInput;
        tile = getTile(rowInput, columnInput);
        number = passedNumber;
        this.setHorizontalAlignment(JTextField.CENTER);
        setBorder(BorderFactory.createLineBorder(new Color(80,80,80), 1));
        if (tile==0 || tile==2 || tile==4 || tile==6 || tile==8) {
            this.setBackground(new Color(250,240,240));
        } else {
        	this.setBackground(new Color(230, 230, 230));
        }
    }
    
    /**
     * Sets the number value held in this square from input
     * @param string
     * 				String representation of the value
     */
    public void setNumber(String string) {
    		this.number = Integer.parseInt(string);
    }
    
    /**
     * Returns the value held within the square
     * @return
     * 			integer value of number within this square
     */
    public int getNumber() {
    		return number;
    }

    /**
     * 
     * @param row
     * @param column
     * @return
     */
    private int getTile(int row, int column) {
        if (row<3) {
            if (column<3) {
                return 0;
            } 
            else if (2<column && column<6) {
                return 1;
            } 
            else {
                return 2;
            }
        } 
        else if (2<row && row<6) {
            if (column<3) {
                return 3;
            } 
            else if (2<column && column<6) {
                return 4;
            } 
            else {
                return 5;
            }
        } 
        else {
            if (column<3) {
                return 6;
            } 
            else if (2<column && column<6) {
                return 7;
            } 
            else {
                return 8;
            }
        }
    }

    /**
     * Compares two square's values
     * @param other
     * 				other square object of who's value this will be compared to 
     * @return
     * 			true if this tile, row, or column is equal to the other's tile, row, or column respectively
     * 			false if the two squares' values are not equal 
     */			
    public boolean compareSquare(Square other) {
        if (this.tile==other.tile || this.row==other.row || this.column==other.column) {
            return true;
        } else {
            return false;
        }
    }
}